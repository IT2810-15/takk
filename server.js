const express = require('express');
const path = require('path');

const app = express();
app.use('/scripts', express.static('scripts'));
app.use('/styles', express.static('styles'));
app.use('/src', express.static('src'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.listen(80, function() {
  console.log('Server listening on port 80');
});

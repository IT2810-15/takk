function unselectAll() {
  $('#lassed').removeClass('selected');
  $('#thomborr').removeClass('selected');
  $('#stianhh').removeClass('selected');
  $('#sindrgar').removeClass('selected');
  $('#stiato').removeClass('selected');
  $('#andrewin').removeClass('selected');
  $('#dok').removeClass('selected');
}

function setSelected(selected) {
  unselectAll();
  $(selected).addClass('selected');
}

function animateChange() {
  $('.pagecontainer').hide().fadeIn(600);
}

$('.navigation').click((event) => {
  const navId = event.target.id;
  $('.pagecontainer').load(`/src/${navId}/index.html`);
  setSelected(`#${navId}`);
  animateChange();
});


$('.pagecontainer').load('/src/lassed/index.html');
setSelected('#lassed');
animateChange();

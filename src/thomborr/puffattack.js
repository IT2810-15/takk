var mouseXposition = 0;
var mouseYposition = 0;
var isRunning = false;
var img = document.getElementById('puff');

$( ".pagecontainer" ).mousemove(function (event) {
	
	if (img != null) {

		mouseXposition = event.clientX;
		mouseYposition = event.clientY;

		
		if(!isRunning){
			init();
			isRunning = true;
		}
	}
});

function init(){
	
	var img = document.getElementById("puff");
	var imgPos = img.getBoundingClientRect();
	img.style.position= 'absolute';
	
	var ix = imgPos.left;
	var iy = imgPos.top;
	
	var toMousePosX = mouseXposition - ix;
	var toMousePosY	= mouseYposition - iy;
	
	var toMousePosLength = Math.sqrt(toMousePosX*toMousePosX + toMousePosY*toMousePosY);
	
	toMousePosX = toMousePosX/toMousePosLength;
	toMousePosY = toMousePosY/toMousePosLength;

	img.style.top = iy + (toMousePosY) * 10 + "px";
	img.style.left = ix + (toMousePosX) * 10 + "px";



	var imgWidth = img.clientWidth;
	var imgHeight = img.clientHeight;

	if(toMousePosX < 0){
		img.src = '/src/thomborr/puff.png';
	}
	else{
		img.src = '/src/thomborr/reversepuff.png';
	}
	
	if(((iy < mouseYposition + 10 && iy >= mouseYposition) || (iy > mouseYposition - imgHeight && iy <= mouseYposition)) && ((ix < mouseXposition + 10 && ix >= mouseXposition) || (ix > mouseXposition - imgWidth && ix <= mouseXposition))){
		document.getElementById('rested').play();
		stop();
	}
	else{
		animate = setTimeout(init , 20);
	}
}
function stop(){
	isRunning = false;
	clearTimeout(animate);
	var img = document.getElementById('puff');
	img.parentNode.removeChild(img);
	var headline = document.getElementById('overskrift');
	headline.innerHTML = "You got rested!";
	headline.style.color = "red";
	var btn = document.getElementById('button');
	btn.style.visibility = "visible";
	var sleepingpuff = document.getElementById("sleepingpuff");
	sleepingpuff.style.visibility = "visible";
	console.log("stopped");
	
	
}
function pause(){
	clearTimeout(animate);
	isRunning = false;
}

function switchPage(){
	$('.pagecontainer').load(`/src/thomborr/index.html`);
	setSelected(`thomborr`);
  	animateChange();
}


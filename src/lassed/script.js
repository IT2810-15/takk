var app = document.getElementById('app');
var list = document.getElementById('elementlist');
var result = document.getElementById('result');
var resultwrapper = document.getElementById('resultwrapper');
var elements = [];

function writeElements(action) {
  let outputHTML = '';
  let index = 0;
  elements.forEach((element) => {
    let newElementClass = ( index === elements.length - 1 && action === 'added') ? 'newelement' : '';
      outputHTML += `<span 
                        class='element ${newElementClass}'
						onclick='removeElement(${index})'>
							✕ ${element.label}
						</span>`;
		index++;
 	})
 	list.innerHTML = outputHTML;
 	resultwrapper.className = (resultwrapper.className === 'fadeinout') ? 'fadeinout2' : 'fadeinout';
 	result.innerHTML = calculateCaffeine(elements);
}

function addElement(argument)  {
	elements.push(getElementObj(argument));
	writeElements('added');
}

function removeElement(elementIndex) {
	elements.splice(elementIndex, 1);
	writeElements('removed');
}

function getElementObj(element) {
    switch(element) {
        case 'filter':
            return ({ label: '1 kopp filterkaffe 140mg', mg: 140 });
		case 'pulver':
			return ({ label: '1 kopp pulverkaffe 100mg', mg: 100 });
		case 'nrg5':
			return ({ label: '1 stor boks energidrikk 160mg', mg: 160 });
		case 'nrg2':
			return ({ label: '1 liten boks energidrikk 80mg', mg: 80 });
		case 'te':
			return ({ label: '1 kopp te 50mg', mg: 50 });
		case 'cola':
			return ({ label: '1 boks Coca Cola 32mg', mg: 32 });
	};
}

function calculateCaffeine(elements) {
  let sum = 0;
  elements.forEach((element) => {
    sum += element.mg;
  });
  return sum;
}

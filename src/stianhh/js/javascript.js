
$('#homebutton').click(function() {
    window.scrollTo(0, 0);
});

$('.content_image').each(function(i, obj) {
    var image = $(obj).find('img')[0];
    $(image).click(function() {
        var description = $(this.parentNode).find('.content_description')[0];
        if (description.style.visibility == 'visible') {
          description.style.visibility = 'hidden';
        }else {
          description.style.visibility = 'visible';
        }
    });
});

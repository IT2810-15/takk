$(() => {
  $('.tab').click((event) => {
    const tabId = event.target.id;
    $('.tab').css('border-bottom', 'outset');
    $(`#${tabId}`).css({'border-bottom': 'solid', 'border-bottom-color': '#FDF3E7'});
    $('.content').hide();
    $(`#${tabId}Content`).show();
  });
});

window.addEventListener('load', dropDown);

function myHoverFunction(idTag) {
	document.getElementById(idTag).style.cursor = 'pointer';
}


function linkToPage(idTag) {
	window.location = "/src/andrewin/"+idTag+".html";
}



function dropDown() {
	var main = document.getElementById("main");
	var elementsInMain = main.getElementsByClassName("dropdown");
	for (var i = 0;  i < elementsInMain.length; i++) {
		var element = elementsInMain[i];
		element.setAttribute('closed', false);
		element.addEventListener('click', function() {
			if(!this.closed) {
				this.style.height = "100%";
			} else {
				this.style.height = "60px";
			}
			this.closed = !this.closed;
		});	

	}
}


$(() => {
	dropDown();
});
